Mon Petit Placement - Test technique Symfony - Dorian CHEREAULT
==============================================

L'application
-------------

Le but de l'exercice est de créer une API complète de Todo-List.

Vous devrez **exposer** une **API**.

### Fonctionnalités

- [✔] Se connecter via un identifiant (exemple : email / mot de passe)
- [✔] Créer une todo-list et des tâches associées
- [✔] Ajouter une tâche dans une todo-list existante
- [✔] Seul le propriétaire de la todo-list peut la supprimer
- [✔] Seul le propriétaire de la tâche ou de la todo-list parente peut la modifier/supprimer
- [✔] Tous les autres utilisateurs peuvent voir les todo-list et tâches des autres
- [✔] Pouvoir filtrer les tâches/todo-list à afficher
- [✔] Bonus : Ajouter un système de status des tâches/todo-list
- [ ] Bonus : Faire un front ? Web ? Mobile ? 



### Technologies

*   Gestionnaire de version : **GIT**
*   Librairies externes ajoutées : **JWT / ApiPlatform / Security**
*   Framework Symfony **5.3.1**
*   Composer **2.0.13**
*   Framework ApiPlatform **2.6.4**
*   **Postman** pour les requêtes API
*   Base de données "todolist" en **PostgreSQL 12**



### Compte Rendu

*   📁 Documentation : https://drive.google.com/file/d/1rfp4Ec2tzYlRDUQOEmKh7U3dX4uvUl1R/view?usp=sharing
*   🕐 Temps passé dessus : 30 heures
*   👉 Justification brève des librairies utilisées :  
                * JWT: Sécurisation de la connexion utilisateur avec un token.  
                * ApiPlatform: Création d'une API plus facilement et rapidement.  
                * Security: Utiliser pour l'authentification des utilisateurs et le hashage du mot de passe.  
*   👽 Difficultés rencontrées : Le token JWT indiquait "invalid credentials", alors que les identifiants étaient correctes. L'erreur vennait d'une incompatibilité de version.


