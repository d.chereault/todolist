<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210613184518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task ALTER status_id SET NOT NULL');
        $this->addSql('ALTER INDEX idx_527edb25ebc2bc9a RENAME TO IDX_527EDB256BF700BD');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE task ALTER status_id DROP NOT NULL');
        $this->addSql('ALTER INDEX idx_527edb256bf700bd RENAME TO idx_527edb25ebc2bc9a');
    }
}
