<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210615235110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tache_todolist (tache_id INT NOT NULL, todolist_id INT NOT NULL, PRIMARY KEY(tache_id, todolist_id))');
        $this->addSql('CREATE INDEX IDX_668CE7E6D2235D39 ON tache_todolist (tache_id)');
        $this->addSql('CREATE INDEX IDX_668CE7E6AD16642A ON tache_todolist (todolist_id)');
        $this->addSql('ALTER TABLE tache_todolist ADD CONSTRAINT FK_668CE7E6D2235D39 FOREIGN KEY (tache_id) REFERENCES tache (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tache_todolist ADD CONSTRAINT FK_668CE7E6AD16642A FOREIGN KEY (todolist_id) REFERENCES todolist (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tache DROP CONSTRAINT fk_93872075ad16642a');
        $this->addSql('DROP INDEX idx_93872075ad16642a');
        $this->addSql('ALTER TABLE tache DROP todolist_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE tache_todolist');
        $this->addSql('ALTER TABLE tache ADD todolist_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tache ADD CONSTRAINT fk_93872075ad16642a FOREIGN KEY (todolist_id) REFERENCES todolist (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_93872075ad16642a ON tache (todolist_id)');
    }
}
