<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210613171924 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task ADD id_to_do_list_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task DROP id_to_do_list');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25D83998E FOREIGN KEY (id_to_do_list_id) REFERENCES todolist (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_527EDB25D83998E ON task (id_to_do_list_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE task DROP CONSTRAINT FK_527EDB25D83998E');
        $this->addSql('DROP INDEX IDX_527EDB25D83998E');
        $this->addSql('ALTER TABLE task ADD id_to_do_list INT NOT NULL');
        $this->addSql('ALTER TABLE task DROP id_to_do_list_id');
    }
}
