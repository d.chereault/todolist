<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210613174727 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task DROP CONSTRAINT fk_527edb2579f37ae5');
        $this->addSql('ALTER TABLE task DROP CONSTRAINT fk_527edb25d83998e');
        $this->addSql('DROP INDEX idx_527edb2579f37ae5');
        $this->addSql('DROP INDEX idx_527edb25d83998e');
        $this->addSql('ALTER TABLE task RENAME COLUMN id_user_id TO user_id');
        $this->addSql('ALTER TABLE task RENAME COLUMN id_to_do_list_id TO todolist_id');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25AD16642A FOREIGN KEY (todolist_id) REFERENCES todolist (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_527EDB25A76ED395 ON task (user_id)');
        $this->addSql('CREATE INDEX IDX_527EDB25AD16642A ON task (todolist_id)');
        $this->addSql('ALTER TABLE todolist DROP CONSTRAINT fk_dd4df6db79f37ae5');
        $this->addSql('DROP INDEX idx_dd4df6db79f37ae5');
        $this->addSql('ALTER TABLE todolist ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE todolist DROP id_user_id');
        $this->addSql('ALTER TABLE todolist ADD CONSTRAINT FK_DD4DF6DBA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DD4DF6DBA76ED395 ON todolist (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE todolist DROP CONSTRAINT FK_DD4DF6DBA76ED395');
        $this->addSql('DROP INDEX IDX_DD4DF6DBA76ED395');
        $this->addSql('ALTER TABLE todolist ADD id_user_id INT NOT NULL');
        $this->addSql('ALTER TABLE todolist DROP user_id');
        $this->addSql('ALTER TABLE todolist ADD CONSTRAINT fk_dd4df6db79f37ae5 FOREIGN KEY (id_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_dd4df6db79f37ae5 ON todolist (id_user_id)');
        $this->addSql('ALTER TABLE task DROP CONSTRAINT FK_527EDB25A76ED395');
        $this->addSql('ALTER TABLE task DROP CONSTRAINT FK_527EDB25AD16642A');
        $this->addSql('DROP INDEX IDX_527EDB25A76ED395');
        $this->addSql('DROP INDEX IDX_527EDB25AD16642A');
        $this->addSql('ALTER TABLE task RENAME COLUMN user_id TO id_user_id');
        $this->addSql('ALTER TABLE task RENAME COLUMN todolist_id TO id_to_do_list_id');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT fk_527edb2579f37ae5 FOREIGN KEY (id_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT fk_527edb25d83998e FOREIGN KEY (id_to_do_list_id) REFERENCES todolist (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_527edb2579f37ae5 ON task (id_user_id)');
        $this->addSql('CREATE INDEX idx_527edb25d83998e ON task (id_to_do_list_id)');
    }
}
