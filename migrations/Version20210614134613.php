<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210614134613 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tache ADD todolist_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tache DROP id_todolist');
        $this->addSql('ALTER TABLE tache ADD CONSTRAINT FK_93872075AD16642A FOREIGN KEY (todolist_id) REFERENCES todolist (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_93872075AD16642A ON tache (todolist_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tache DROP CONSTRAINT FK_93872075AD16642A');
        $this->addSql('DROP INDEX IDX_93872075AD16642A');
        $this->addSql('ALTER TABLE tache ADD id_todolist INT NOT NULL');
        $this->addSql('ALTER TABLE tache DROP todolist_id');
    }
}
