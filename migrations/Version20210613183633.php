<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210613183633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task DROP CONSTRAINT fk_527edb256bf700bd');
        $this->addSql('DROP INDEX idx_527edb256bf700bd');
        $this->addSql('ALTER TABLE task RENAME COLUMN status_id TO id_status_id');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25EBC2BC9A FOREIGN KEY (id_status_id) REFERENCES status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_527EDB25EBC2BC9A ON task (id_status_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE task DROP CONSTRAINT FK_527EDB25EBC2BC9A');
        $this->addSql('DROP INDEX IDX_527EDB25EBC2BC9A');
        $this->addSql('ALTER TABLE task RENAME COLUMN id_status_id TO status_id');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT fk_527edb256bf700bd FOREIGN KEY (status_id) REFERENCES status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_527edb256bf700bd ON task (status_id)');
    }
}
