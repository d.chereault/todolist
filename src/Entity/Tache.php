<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TacheRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TacheRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"idUser": "exact", "idStatus": "exact","title":"partial"})
 */
class Tache
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:todolist:Post"})
     */
    private $id;

   /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Groups({"read:todolist:Post"})
     * @var UserInterface
     */
    private $idUser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Todolist", inversedBy="taches")
     * @ORM\JoinColumn(nullable=false, name="todolist_id", referencedColumnName="id")
     * @var Todolist
     */
    private $idTodolist;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * @Groups({"read:todolist:Post"})
     * @var Status
     */
    private $idStatus;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:todolist:Post"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"read:todolist:Post"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:todolist:Post"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:todolist:Post"})
     */
    private $description;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): UserInterface
    {
        return $this->idUser;
    }

    public function setIdUser(UserInterface $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getIdTodolist(): ?Todolist
    {
        return $this->idTodolist;
    }

    public function setIdTodolist(?Todolist $idTodolist): self
    {
        $this->idTodolist = $idTodolist;

        return $this;
    }

    public function getIdStatus(): ?Status
    {
        return $this->idStatus;
    }

    public function setIdStatus(?Status $idStatus): self
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
