<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\TodolistRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


/**
 * @ApiResource(
 *     normalizationContext={
 *              "groups"={"read:todolist:collection"}},
 *     itemOperations={
 *          "get"= {
 *              "normalization_context"={"groups"= {"read:todolist:collection","read:todolist:Post"}}
 *          },
 *          "put",
 *          "delete"
 *     }
 * )
 * @ORM\Entity(repositoryClass=TodolistRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"idUser": "exact", "title":"partial"})
 */
class Todolist
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:todolist:collection"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true, name="user_id", referencedColumnName="id")
     * @Groups({"read:todolist:collection"})
     * @var UserInterface
     */
    private $idUser;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:todolist:collection"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"read:todolist:collection"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:todolist:collection"})
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tache", mappedBy="idTodolist", orphanRemoval=true)
     * @Groups({"read:todolist:collection"})
     */
    private $taches;

    public function __construct()
    {
        $this->taches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?UserInterface
    {
        return $this->idUser;
    }

    public function setIdUser(?UserInterface $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Tache[]
     */
    public function getTaches(): Collection
    {
        return $this->taches;
    }

    public function addTach(Tache $tach): self
    {
        if (!$this->taches->contains($tach)) {
            $this->taches[] = $tach;
            $tach->setIdTodolist($this);
        }

        return $this;
    }

    public function removeTach(Tache $tach): self
    {
        if ($this->taches->removeElement($tach)) {
            // set the owning side to null (unless already changed)
            if ($tach->getIdTodolist() === $this) {
                $tach->setIdTodolist(null);
            }
        }

        return $this;
    }
}
