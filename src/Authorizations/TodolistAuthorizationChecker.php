<?php

declare(strict_types=1);

namespace App\Authorizations;

use App\Entity\Todolist;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class TodolistAuthorizationChecker
{
    //method Allowed for request
    private $methodAllowed = [
        Request::METHOD_POST,
        Request::METHOD_DELETE,
        Request::METHOD_PATCH,
        Request::METHOD_PUT,
    ];

    /**
     * @var User
     */
    private $user;

    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    // Authorizations check : method / user's id with todolist's owner id
    public function check(Todolist $todolist, string $method): void
    {
       $this->isAuthenticated();
       if($this->isMethodAllowed($method) || $todolist->getIdUser()->getId() !== $this->user->getId()
       ){
        $errorMessage="It's not your resource";
        throw new UnauthorizedHttpException($errorMessage, $errorMessage);
       }
    }

    // Indicates whether the user is logged in
    public function isAuthenticated(){
        if(null === $this->user){
            $errorMessage=" You are not Authenticated";
            throw new UnauthorizedHttpException($errorMessage, $errorMessage);
        }
    }

    // Return false if method is in array
    public function isMethodAllowed(string $method): bool
    {
        return !in_array($method, $this->methodAllowed, true);
    }
}
