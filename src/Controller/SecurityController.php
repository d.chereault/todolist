<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends AbstractController {

    public function login(){

            //Checked if user is valid
            $user = $this->getUser();

            //give some informations of this user
            return $this->json([
                'username' => $user->getUsername(),
                'roles' => $user->getRoles(),
            ]);
        
    }
}