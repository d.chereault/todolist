<?php

declare(strict_types=1);

namespace App\Events;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\viewEvent;
use Symfony\Component\HttpFoundation\Request;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Authorizations\TacheAuthorizationChecker;
use App\Entity\Tache;

class TacheSubscriber implements EventSubscriberInterface
{
    //Method not allowed for modification
    private $methodNotAllowed = [
        Request::METHOD_GET,
    ];

    private $tacheAuthorizationChecker;
    public function __construct(TacheAuthorizationChecker $tacheAuthorizationChecker){
        $this->tacheAuthorizationChecker= $tacheAuthorizationChecker;
    }

    //Check if method is not in array and if tache is an instance of class Tache
    public function check(viewEvent $event)
    {
        $tache= $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($tache instanceof Tache && !in_array($method, $this->methodNotAllowed, true)
          ){
            $this->tacheAuthorizationChecker->check($tache, $method);
            $tache->setUpdatedAt(new \DateTimeImmutable());
        }
    }

    //Event
    public static function getSubscribedEvents()
    {
        return [
            'kernel.view' => ['check', EventPriorities::PRE_VALIDATE],
        ];
    }

}