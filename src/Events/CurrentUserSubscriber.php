<?php

namespace App\Events;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpFoundation\Request;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Tache;
use App\Entity\Todolist;

class CurrentUserSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security= $security;
    }

    //Event
    public static function  getSubscribedEvents()
    {
        return [
            'kernel.view' => ['currentUser', EventPriorities::PRE_VALIDATE]
        ];
    }

    //In the json, the function completes with the logged in user.
    public function currentUser(ViewEvent $event)
    {
        $content = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($content instanceof Tache && $method==Request::METHOD_POST){
            $content->setIdUser($this->security->getUser());
        }

        if($content instanceof Todolist && $method==Request::METHOD_POST){
            $content->setIdUser($this->security->getUser());
        }
    }
}