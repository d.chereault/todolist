<?php

namespace App\Events;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\viewEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;

class PasswordEncoderSubscriber implements EventSubscriberInterface
{
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder=$passwordEncoder;
    }

    //Encode password before it's defined in database
    public function cryptPassword(viewEvent $event)
    {
        $entity= $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($entity instanceof User && $method == Request::METHOD_POST){
            $entity->setPassword($this->passwordEncoder->encodePassword(
                $entity,
                $entity->getPassword()
            ));
        }
    }

    //Event
    public static function getSubscribedEvents()
    {
        return [
            'kernel.view' => ['cryptPassword', EventPriorities::PRE_WRITE],
        ];
    }

}