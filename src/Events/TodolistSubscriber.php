<?php

declare(strict_types=1);

namespace App\Events;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\viewEvent;
use Symfony\Component\HttpFoundation\Request;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Authorizations\TodolistAuthorizationChecker;
use App\Entity\Todolist;

class TodolistSubscriber implements EventSubscriberInterface
{
    //Method not allowed for modification
    private $methodNotAllowed = [
        Request::METHOD_GET,
    ];

    private $todolistAuthorizationChecker;
    public function __construct(TodolistAuthorizationChecker $todolistAuthorizationChecker){
        $this->todolistAuthorizationChecker= $todolistAuthorizationChecker;
    }

    public function check(viewEvent $event)
    {
        $todolist= $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($todolist instanceof Todolist && !in_array($method, $this->methodNotAllowed, true)
          ){
            $this->todolistAuthorizationChecker->check($todolist, $method);
            $todolist->setUpdatedAt(new \DateTimeImmutable());
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.view' => ['check', EventPriorities::PRE_VALIDATE],
        ];
    }

}